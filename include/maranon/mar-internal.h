//  
//  mar-internal.h
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2011 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
#ifndef maranon_mar_internal_h
#define maranon_mar_internal_h

#include <stddef.h>

#if defined(__GNU__) || defined(MARANON_TARGET_OS_WIN32)
#   include <stdint.h>
#endif  /* defined(__GNU__) || defined(MARANON_TARGET_OS_WIN32) */


#if !defined(MAR_EXTERN_C_BEGIN)
#   ifdef __cplusplus
#       define MAR_EXTERN_C_BEGIN extern "C" {
#       define MAR_EXTERN_C_END   }
#   else
#       define MAR_EXTERN_C_BEGIN
#       define MAR_EXTERN_C_END
#   endif   /* def __cplusplus */
#endif  /* !defined(MAR_EXTERN_C_BEGIN) */

#if defined(MARANON_TARGET_OS_WIN32)
#   if !defined(MAR_EXPORT)
#       if defined(MARANON_BUILDING) && defined(__cplusplus)
#           define MAR_EXPORT extern "C" __declspec(dllexport)
#       elif defined(MARANON_BUILDING) && !defined(__cplusplus)
#           define MAR_EXPORT extern __declspec(dllexport)
#       elif defined(__cplusplus)
#           define MAR_EXPORT extern "C" __declspec(dllimport)
#       elif !defined(__cplusplus)
#           define MAR_EXPORT extern __declspec(dllimport)
#       else
#           define MAR_EXPORT
#       endif  /* defined(MARANON_BUILDING) && defined(__cplusplus) */
#   endif  /* not defined(MAR_EXPORT) */
#else
#   define MAR_EXPORT extern
#endif  /* defined(MARANON_TARGET_OS_WIN32) */

MAR_EXTERN_C_BEGIN

#if !defined(NULL)
#   if defined(__GNUG__)
#       define NULL __null
#   elif defined(__cplusplus)
#       define NULL 0
#   else
#       define NULL ((void *)0)
#   endif  /* defined(__GNUG__) */
#endif  /* !defined(NULL) */

typedef unsigned char MARBoolean;

#ifndef TRUE
#   define TRUE 1
#endif  /* ndef TRUE */

#ifndef FALSE
#   define FALSE 0
#endif  /* ndef FALSE */

#if !defined(MAR_INLINE)
#   if defined(__GNUC__) && ( __GNUC__ == 4 ) && !defined(DEBUG)
#       defined MAR_INLINE static __inline__ __attribute__((always_inline))
#   elif defined(__GNUC__)
#       defined MAR_INLINE static __inline__
#   elif defined(__MWERKS__) || defined(__cplusplus)
#       defined MAR_INLINE static inline
#   elif defined(MARANON_TARGET_OS_WIN32)
#       defined MAR_INLINE static __inline__
#   endif  /* defined(__GNUC__) && ( __GNUC__ == 4 ) && !defined(DEBUG) */
#endif  /* !defined(MAR_INLINE) */

typedef unsigned int MARClassID;
typedef unsigned int MARBitField;
typedef unsigned int MARIndex;
typedef unsigned int MARCount;
typedef unsigned int MARVolume;
typedef unsigned int MARHashCode;

typedef const void* MARValue_ref;


typedef const struct mar_string* MARConstantString_ref;
typedef       struct mar_string* MARMutableString_ref;

#define MARString_ref MARConstantString_ref


enum __MARComparisonResults {
    MARComparisonResults_LessThan    = -1,  //!< left が right より小さい時に返されます。
    MARComparisonResults_SameAs      =  0,  //!< left が right と等しい時に返されます。
    MARComparisonResults_GreaterThan =  1,  //!< left が right より大きい時に返されます。
}
typedef int MARComparisonResult;


typedef MARComparisonResult (*MARComparisonFunction)(id left, id right);


enum __MARAccessResults {
    MARAccessResults_NotFound = -1
};


struct MARRange {
    int first;
    int last;
};


#ifdef MAR_INLINE
MAR_INLINE MARRange MARRange_new(int first, int last)
{
    MARRange new;

    new.first = first;
    new.last = last;

    return new;
}
#else
#   define  MARRange_new(_first_, _last_)  __MARRange_new(_first_, _last_)
#endif  /* def MAR_INLINE */


MAR_EXPORT MARRange __MARRange_new(int first, int last);


/*!
 *
 */
MAR_EXPORT id MAR_retain(id self);


/*!
 *
 */
MAR_EXPORT MARBoolean MAR_tryRetain(id self);


/*!
 *
 */
MAR_EXPORT void MAR_release(id self);


/*!
 *
 */
MAR_EXPORT MARBoolean MAR_isDeallocating(id self);


/*!
 *
 */
MAR_EXPORT MARBoolean MAR_equals(id left, id right);


/*!
 *
 */
MAR_EXPORT MARBoolean MAR_identityEquals(id left, id right);


#define INVOKE_CALLBACK0(_func_)                                           (*_func_)()
#define INVOKE_CALLBACK1(_func_, _arg0_)                                   (*_func_)( _arg0_ )
#define INVOKE_CALLBACK2(_func_, _arg0_, _arg1_)                           (*_func_)( _arg0_, _arg1_ )
#define INVOKE_CALLBACK3(_func_, _arg0_, _arg1_, _arg2_)                   (*_func_)( _arg0_, _arg1_, _arg2_ )
#define INVOKE_CALLBACK4(_func_, _arg0_, _arg1_, _arg2_, _arg3_)           (*_func_)( _arg0_, _arg1_, _arg2_, _arg3_ )
#define INVOKE_CALLBACK5(_func_, _arg0_, _arg1_, _arg2_, _arg3_, _arg4_)   (*_func_)( _arg0_, _arg1_, _arg2_, _arg3_, _arg4_ )


MAR_EXTERN_C_END


#endif  /* maranon_mar_internal_h */
