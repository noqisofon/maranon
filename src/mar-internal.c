//  
//  mar-nternal.c
//  
//  Auther:
//       ned rihine <ned.rihine@gmail.com>
// 
//  Copyright (c) 2011 rihine All rights reserved.
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include "maranon/mar-nternal.h"


MAR_EXPORT id MAR_retain(id self)
{
}


MAR_EXPORT MARBoolean MAR_tryRetain(id self)
{
}


MAR_EXPORT void MAR_release(id self)
{
}


MAR_EXPORT MARBoolean MAR_isDeallocating(id self)
{
}


MAR_EXPORT MARBoolean MAR_equals(id left, id right)
{
}


MAR_EXPORT MARBoolean MAR_identityEquals(id left, id right)
{
    return left == right;
}
